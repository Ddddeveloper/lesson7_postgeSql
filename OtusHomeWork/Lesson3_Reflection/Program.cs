﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading;
using Lesson7_Db.models;

namespace Lesson7_Db
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new App();
            app.AppStart();
        }
    }

    public class App
    {
        private bool isWork;
        private AppDbConnector worker;
        
        public App()
        {
            this.isWork = true;
            
            //Тут можно задать конструктор с параметрами сервер, порт, юзер и пароль для формирования своей строки подключения
            this.worker = new AppDbConnector();
        }

        public void AppStart()
        {
            while (isWork)
            {
                Console.Clear();
                Console.WriteLine("Демонстрация работы");

                Console.WriteLine(this.worker.CreatAvitoDB());
                Console.WriteLine(this.worker.CreateUsers());
                Console.WriteLine(this.worker.CreateLots());
                Console.WriteLine(this.worker.CreateDeals());
                var data = this.worker.ViewData();
                
                Console.WriteLine();
                
                Console.WriteLine("Отчет по сделкам");
                foreach (var deal in data)
                {
                    Console.WriteLine($"Пользователь: {deal.UserName}, лот: {deal.Item}, дата лота: {deal.LotDate}, цена: {deal.Price}, дата сделки: {deal.DealDate}");
                }
                Console.WriteLine();
                Console.WriteLine("Нажмите 1 что бы удалить базу данных и повторить заново, любую другую клавишу для выхода из программы");
                
                var input = Console.ReadKey().KeyChar.ToString();
                
                int val;
                
                if (!Int32.TryParse(input, out val) || val != 1)
                {
                    ShutDown();
                }
                else if (val == 1)
                {
                    this.worker.DeleteDb();
                    
                    Console.WriteLine("Нажмите людую клавишу, что бы продолжить");
                    
                    Console.ReadKey();
                    
                    Console.Clear();
                }
            }
        } 

        private void ShutDown()
        {
            Console.WriteLine();
            Console.WriteLine("Работа программы будет завершена через 3 сек");
            Thread.Sleep(1000);
            Console.WriteLine("Работа программы будет завершена через 2 сек");
            Thread.Sleep(1000);
            Console.WriteLine("Работа программы будет завершена через 1 сек");
            Thread.Sleep(1000);
            this.isWork = false;
        }
    }
}
