﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7_Db.models
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class Lot
    {
        public int Id { get; set; }

        public string Item { get; set; }
    }

    public class Deal
    {
        public int Id { get; set; }

        public int LotId { get; set; } 

        public DateTime DealDate { get; set; }
    }

    public class ViewData
    {
        public string UserName { get; set; }

        public string Item { get; set; }

        public DateTime LotDate { get; set; }

        public DateTime DealDate { get; set; }

        public Decimal Price { get; set; }
    }
}
