﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Npgsql;
using Lesson7_Db.models;

namespace Lesson7_Db
{
    public class AppDbConnector
    {
        private string connectionSting;
        

        public AppDbConnector()
        {
            connectionSting = $"Server=localhost;Username=postgres;Port=5432;Password=7702430046";
        }

        public AppDbConnector(string server, string port, string user, string pass)
        {
            connectionSting = $"Server={server};Username={user};Port={port};Password={pass}";
        }

        //Создание БД
        public string CreatAvitoDB()
        {
            var result = string.Empty;
            try
            {
                using (var conn = new NpgsqlConnection(connectionSting))
                {
                    conn.Open();

                    using (var comm = new NpgsqlCommand($"CREATE DATABASE avitodb", conn))
                    {
                        comm.ExecuteNonQuery();
                        result = $"База данных 'avitodb' создана";
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                this.connectionSting += ";Database=avitodb";
            }
        }

        //Создание тарблицы пользователей
        public string CreateUsers()
        {
            var result = string.Empty;
            try
            {
                using (var conn = new NpgsqlConnection(connectionSting))
                {
                    conn.Open();

                    //Удаление тестовой таблицы пользователей,если существует
                    using (var command = new NpgsqlCommand("DROP TABLE IF EXISTS users", conn))
                    {
                        command.ExecuteNonQuery();
                    }

                    //Создание таблицы пользователей
                    using (var comm = new NpgsqlCommand($"CREATE TABLE users(id serial PRIMARY KEY, name VARCHAR(50))", conn))
                    {
                        comm.ExecuteNonQuery();
                    }

                    //Добавление пользователей
                    using (var comm = new NpgsqlCommand("INSERT INTO users (name) VALUES (@n1), (@n2), (@n3), (@n4), (@n5)", conn))
                    {
                        comm.Parameters.AddWithValue("n1", "Петр");
                        comm.Parameters.AddWithValue("n2", "Илья");
                        comm.Parameters.AddWithValue("n3", "Фёкла");
                        comm.Parameters.AddWithValue("n4", "Никонор");
                        comm.Parameters.AddWithValue("n5", "Агафья");
                        comm.ExecuteNonQuery();

                        result = "Пользователи добавлены";
                    }

                }

                return result;

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //Создание тарблицы лотов
        public string CreateLots()
        {
            var result = string.Empty;
            try
            {
                using (var conn = new NpgsqlConnection(connectionSting))
                {
                    conn.Open();

                    //Удаление тестовой таблицы лотов,если существует
                    using (var comm = new NpgsqlCommand("DROP TABLE IF EXISTS lots", conn))
                    {
                        comm.ExecuteNonQuery();
                    }

                    //Создание таблицы lots
                    using (var comm = new NpgsqlCommand($"CREATE TABLE lots(id serial PRIMARY KEY, " +
                        $"item VARCHAR(50)," +
                        $"lotDate DATE," +
                        $"userId INTEGER," +
                        $"price MONEY," +
                        $"FOREIGN KEY (userId) REFERENCES users (id) ON DELETE CASCADE)", conn))
                    {
                        comm.ExecuteNonQuery();
                    }

                    //Получение данных уже записанных пользователей
                    var users = new List<User>();
                    using (var comm = new NpgsqlCommand("SELECT * from users", conn))
                    {
                        var reader = comm.ExecuteReader();
                        while (reader.Read())
                        {
                            users.Add(new User
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1)
                            });
                        }
                        reader.Close();
                    }

                    //Заполнение таблицы lot данными
                    //Добавление лотов
                    using (var comm = new NpgsqlCommand("INSERT INTO lots (item, lotDate, userId, price) VALUES" +
                        "(@n1, @d1, @u1, @p1)," +
                        "(@n2, @d2, @u2, @p2)," +
                        "(@n3, @d3, @u3, @p3)," +
                        "(@n4, @d4, @u4, @p4)", conn))
                    {
                        var random = new Random();

                        comm.Parameters.AddWithValue("n1", "Трюмо от бабки");
                        comm.Parameters.AddWithValue("d1", new DateTime(2021, 10, 7));
                        comm.Parameters.AddWithValue("u1", random.Next(1, users.Count() +1));
                        comm.Parameters.AddWithValue("p1", 10500);

                        comm.Parameters.AddWithValue("n2", "Велосипед без седла");
                        comm.Parameters.AddWithValue("d2", new DateTime(2021, 9, 15));
                        comm.Parameters.AddWithValue("u2", random.Next(1, users.Count() + 1));
                        comm.Parameters.AddWithValue("p2", 8000);

                        comm.Parameters.AddWithValue("n3", "Патифон антикварный");
                        comm.Parameters.AddWithValue("d3", new DateTime(2021, 8, 3));
                        comm.Parameters.AddWithValue("u3", random.Next(1, users.Count() + 1));
                        comm.Parameters.AddWithValue("p3", 12300);

                        comm.Parameters.AddWithValue("n4", "Платье женское");
                        comm.Parameters.AddWithValue("d4", new DateTime(2021, 7, 9));
                        comm.Parameters.AddWithValue("u4", random.Next(1, users.Count() + 1));
                        comm.Parameters.AddWithValue("p4", 6500);

                        comm.ExecuteNonQuery();

                        result = "Лоты добавлены";
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //Создание тарблицы сделок
        public string CreateDeals()
        {
            var result = string.Empty;
            try
            {
                using (var conn = new NpgsqlConnection(connectionSting))
                {
                    conn.Open();

                    //Удаление тестовой таблицы сделок, если существует
                    using (var comm = new NpgsqlCommand("DROP TABLE IF EXISTS deals", conn))
                    {
                        comm.ExecuteNonQuery();
                    }

                    //Создание таблицы deals
                    using (var comm = new NpgsqlCommand($"CREATE TABLE deals(id serial PRIMARY KEY, " +
                        $"dealDate DATE," +
                        $"lotId INTEGER," +
                        $"FOREIGN KEY (lotId) REFERENCES lots (id) ON DELETE CASCADE)", conn))
                    {
                        comm.ExecuteNonQuery();
                    }

                    //Получение данных уже записанных лотов
                    var lots = new List<Lot>();
                    using (var comm = new NpgsqlCommand("SELECT * from lots", conn))
                    {
                        var reader = comm.ExecuteReader();
                        while (reader.Read())
                        {
                            lots.Add(new Lot
                            {
                                Id = reader.GetInt32(0),
                                Item = reader.GetString(1)
                            });
                        }
                        reader.Close();
                    }

                    //Заполнение таблицы lot данными
                    //Добавление сделок
                    using (var comm = new NpgsqlCommand("INSERT INTO deals (dealDate, lotId) VALUES" +
                        "(@d1, @l1)," +
                        "(@d2, @l2)", conn))
                    {
                        var random = new Random();

                        comm.Parameters.AddWithValue("d1", new DateTime(2021, 10, 7));
                        comm.Parameters.AddWithValue("l1", random.Next(1, lots.Count() + 1));

                        comm.Parameters.AddWithValue("d2", new DateTime(2021, 10, 7));
                        comm.Parameters.AddWithValue("l2", random.Next(1, lots.Count() + 1));

                        comm.ExecuteNonQuery();

                        result = "Сделки добавлены";
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //Удаление таблицы
        public void DeleteDb()
        {
            try
            {
                using (var conn = new NpgsqlConnection(connectionSting))
                {
                    conn.Open();
                    
                    //Удаление тестовой таблицы сделок, если существует
                    using (var comm = new NpgsqlCommand("DROP DATABASE IF EXISTS avitodb WITH FORCE", conn))
                    {
                        comm.ExecuteNonQuery();
                    }

                    Console.Out.WriteLine("База данных уничтожена!"); 
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
        }

        public List<ViewData> ViewData()
        {
            var result = new List<Lesson7_Db.models.ViewData>();
            try
            {
                using (var conn = new NpgsqlConnection(connectionSting))
                {
                    conn.Open();

                    //Получение составных данных
                    using (var comm = new NpgsqlCommand("SELECT users.name, lots.lotdate, lots.price, lots.item, deals.dealdate FROM users, lots, deals WHERE deals.lotid = lots.id and lots.userid = users.id"
                        , conn))
                    {
                        var reader = comm.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Add(new ViewData
                            {
                                UserName = reader.GetString(0),
                                LotDate = reader.GetDateTime(1),
                                Item = reader.GetString(3),
                                Price = reader.GetDecimal(2),
                                DealDate = reader.GetDateTime(4),
                            });
                        }
                        reader.Close();
                    }

                }
            }
            catch(Exception ex) {
                Console.Out.WriteLine(ex.Message);
            }

            return result;
        }
    }

}
